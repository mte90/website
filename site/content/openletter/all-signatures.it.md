---
title: "Tutti i firmatari pubblici"
type: page
layout: subpage
---

Questa è la lista completa di tutte le persone che hanno firmato la lettera aperta e concordano all'avere il loro nome in pubblico. {{< count type="signatures" >}} persone hanno espresso il già il loro supporto, sei tra di loro?

Questa lista è aggiornata ogni ora.

{{< show_signatures >}}

