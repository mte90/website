---
title: "Grazie per la firma!"
type: page
layout: subpage
---

## La tua firma è confermata

Grazie per aver firmato la lettera aperta "Public Money, Public Code". Il tuo supporto significa molto per noi. 

Se hai scelto di mostrare il tuo nome nella lista pubblica il tuo nome apparirà nell'[elenco delle firme](../all-signatures) nella prossima ora. Se hai scelto di ricevere maggiori informazioni  ti terremo aggiornato riguardo novità riguardo questa campagna via email.

## Prossimi passaggi

Aiutaci lasciando una tua firma e [condividendo questa campagna](../../#spread) con i tuoi amici. Insieme possiamo incoraggiare chi decide nelle pubbliche amministrazioni e politici nel fare nuovi software che seguano le licenze "Free and Open Source Software" ed avviare uno standard nel settore pubblico in tutta Europa.

Puoi anche [ordinare adesivi e materiale informativo](https://fsfe.org/promo#pmpc) dalla Free Software Foundation Europe.

Fare Free Software in Europa è importante e pensa se [supportare la FSFE](https://fsfe.org/donate/?pmpc) o una delle [associazioni partner](../../#organisations).

Con il tuo supporto possiamo aiutare chi decide in tutta Europa nel capire che pubblicare il codice sorgete sotto licenze "Free and Open Source Software" sia la soluzione migliore per loro, per altre amministrazioni, compagnie e specialmente la cosa pubblica in generale. 
